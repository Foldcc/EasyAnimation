﻿using UnityEngine;

namespace EasyAnimation {

    [AddComponentMenu("EasyAnimation/缩放效果")]
    public class EasyAnimation_Enlarge : EasyAnimationTemplateMethod
    {
        [Header("起始大小")]
        public Vector3 startScale = Vector3.zero;
        [Header("结束大小")]
        public Vector3 endScale = Vector3.one;

        protected override void Easy_Animation_Awake()
        {
            //rectSize = transform.localScale;
        }

        protected override void PrimitiveOperation_Start()
        {
            ead = new EaseAinmationDrive(1, 0, 1, easetype);
        }

        protected override bool PrimitiveOperation_UpDate(float time)
        {
            transform.localScale = startScale + (endScale - startScale) * ead.getProgress(time) ;
            return true;
        }

        public override void Rese()
        {
            transform.localScale = startScale;
        }
    }
}
