﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EasyAnimation {
    [AddComponentMenu("EasyAnimation/TimeDrive/UpdateDrive")]
    public class EasyTimeDriveBase : EasyTimeDrive
    {
        private void Update()
        {
            if (animtaionDrive != null)
            {
                animtaionDrive.Invoke(Time.deltaTime * TimeScale);
            }
        }
    }
}

