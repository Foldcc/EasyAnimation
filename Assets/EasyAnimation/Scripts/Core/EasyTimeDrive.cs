﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EasyAnimation {

    public abstract class EasyTimeDrive : MonoBehaviour, IEasyTimeDrive
    {
        [Range(0 , 10.0f) , Header("驱动器时间速率，影响所有使用该驱动器的动画")]
        public float TimeScale = 1;

        protected Action<float> animtaionDrive;

        public void Init()
        {
        }

        public void AddAction(Action<float> action)
        {
            if (animtaionDrive != null)
            {
                animtaionDrive += action;
            }
            else {
                animtaionDrive = action;
            }
        }

        public void RemoveAction(Action<float> action)
        {
            if (animtaionDrive != null)
            {
                animtaionDrive -= action;
            }
        }
    }
}

