﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EasyAnimation {
    public interface IEasyTimeDrive
    {
        void Init();

        void AddAction(Action<float> action);

        void RemoveAction(Action<float> action);
    }
}


