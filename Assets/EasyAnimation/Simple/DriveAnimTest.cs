﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyAnimation;

public enum DriveType {
    MiniDrive,
    GlobaDrive
}

public class DriveAnimTest : MonoBehaviour
{
    public EasyAnimationTemplateMethod templateMethod;
    public EasyTimeDriveBase easyTimeDriveBase;

    public DriveType driveType;

    public int objecyCount;

    public Transform root;

    private List<EasyAnimationTemplateMethod> easyAnimationTemplateMethods = new List<EasyAnimationTemplateMethod>();

    public void Create() {
        for (int i = 0; i < 10000; i++)
        {
            var e = Instantiate(templateMethod, root);
            if (driveType == DriveType.GlobaDrive)
            {
                e.TimeDrive = easyTimeDriveBase;
            }
            else {
                e.TimeDrive = null;
            }
            e.isLoop = true;
            e.Play();
            easyAnimationTemplateMethods.Add(e);
        }
    }

    public void Clear() {
        foreach (var item in easyAnimationTemplateMethods)
        {
            Destroy(item.gameObject);
        }
        easyAnimationTemplateMethods.Clear();
    }
}
